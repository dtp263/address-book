﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using address_book;

namespace address_book_test
{
    [TestClass]
    public class ToStringsTests
    {
        [TestMethod]
        public void Phone_Number_To_String()
        {
            var sample = new Phone_Number() { area_code = "340", first = "642", last = "4604"};
            Assert.AreEqual("(340) 642-4604", sample.ToString());
        }
        [TestMethod]
        public void Address_To_String()
        {
            var sample = new Address() { house_number = "32", street = "S. Prospect", city = "Burlington", state="vt", zip="05401" };
            Assert.AreEqual("32 S. Prospect\nBurlington, vt 05401", sample.ToString());
        }
        [TestMethod]
        public void Person_To_String()
        {
            var samplePhone = new Phone_Number() { area_code = "340", first = "642", last = "4604" };
            var sampleAddress = new Address() { house_number = "32", street = "S. Prospect", city = "Burlington", state = "vt", zip = "05401" };
            var samplePerson = new Person() { name="Olin", address=sampleAddress, phone_number=samplePhone};
            Assert.AreEqual("Olin\n32 S. Prospect\nBurlington, vt 05401\n(340) 642-4604", samplePerson.ToString());
        }
    }
    
    
    [TestClass]
    public class ToXMLTests
    {
        [TestMethod]
        public void Phone_Number_To_XML()
        {
            var sample = new Phone_Number() { area_code = "340", first = "642", last = "4604" };
            Assert.AreEqual("\t<phone_number>\n\t\t<area_code>340</area_code>\n\t\t<first>642</first>\n\t\t<last>4604</last>\n\t<phone_number>\n", sample.toXML());
        }
        [TestMethod]
        public void Address_To_XML()
        {
            var sample = new Address() { house_number = "32", street = "S. Prospect", city = "Burlington", state = "vt", zip = "05401" };
            Assert.AreEqual("<address>\n\t\t<house_number>32</house_number>\n\t\t<street>S. Prospect</street>\n\t\t<city>Burlington</city>\n\t\t<state>vt</state>\n\t\t<zip>05401</zip>\n\t<address>\n", sample.toXML());
        }
        [TestMethod]
        public void Person_To_XML()
        {
            var samplePhone = new Phone_Number() { area_code = "340", first = "642", last = "4604" };
            var sampleAddress = new Address() { house_number = "32", street = "S. Prospect", city = "Burlington", state = "vt", zip = "05401" };
            var samplePerson = new Person() { name = "Olin", address = sampleAddress, phone_number = samplePhone }; 
            Assert.AreEqual("<address>\n\t\t<house_number>32</house_number>\n\t\t<street>S. Prospect</street>\n\t\t<city>Burlington</city>\n\t\t<state>vt</state>\n\t\t<zip>05401</zip>\n\t<address>\n", sample.toXML());
            
            "<person>\n\t<name>Olin</name>\n\t" + address.toXML() + phone_number.toXML() + "</person>\n"
        }
    }
}
