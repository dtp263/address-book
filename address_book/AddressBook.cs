﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace address_book
{

    // The AddressBook class is used to manipulate the "contacts" collection.

    public class AddressBook
    {
        public List<Person> contacts { get; set; }

        public void addContact(Person newPerson)
        {
            if (contacts == null)
                contacts.Add(newPerson);
            else if (contacts.Any(Person => contacts.Contains(newPerson)))
            {
                Console.Write("Im sorry this person already exists in your contacts...\nPress enter to return to the main menu.");
                Console.ReadKey();
            }
            else
            {
                contacts.Add(newPerson);
            }
        }

        public List<Person> searchContacts(string query, string searchType)
        {
            List<Person> results;
            switch (searchType)
            { 
                case "city":    
                    results = contacts.FindAll(Person => Person.address.city == query);
                    break;
                case "state":
                    results = contacts.FindAll(Person => Person.address.state == query);
                    break;
                case "zip":
                    results = contacts.FindAll(Person => Person.address.zip == query);
                    break;
                case "name":
                    results = contacts.FindAll(Person => Person.name == query);
                    break;
                case "phone_number":
                    results = contacts.FindAll(Person => Person.phone_number.ToString() == query);
                    break;
                default:
                    results = null;
                    break;
            }
            return results;
        }
    }
}
