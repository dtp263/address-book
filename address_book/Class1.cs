﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace address_book
{
    public class Address
    {
        public string house_number { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }

        public override string ToString()
        {
            return house_number + " " + street + "/n" + city + ", " + state + zip;
        }
    }
}
