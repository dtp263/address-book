﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace address_book
{
    interface IAddress
    {
        string toXML();
        string toFlatText();
    }

    public class Address : IAddress
    {
        public string house_number { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }

        public override string ToString()
        {
            return house_number + " " + street + "\n" + city + ", " + state + " " + zip;
        }

        public string toXML()
        {
            return "<address>\n\t\t<house_number>" + house_number + "</house_number>\n\t\t<street>" + street + "</street>\n\t\t<city>" + city + "</city>\n\t\t<state>" + state + "</state>\n\t\t<zip>" + zip + "</zip>\n\t<address>\n";
        }

        public string toFlatText()
        {
            return house_number + "\n" + street + "\n" + city + "\n" + state + "\n" + zip + "\n";
        }
    }
}
