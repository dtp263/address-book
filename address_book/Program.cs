﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace address_book
{
    public class Program
    {
        static void Main(string[] args)
        {
            List<Person> sampleContacts = new List<Person>();
            sampleContacts.Add(new Person()
            {
                name = "Olin",
                address = new Address()
                    {
                        house_number = "321",
                        street = "Prospect",
                        city = "Burlington",
                        state = "VT",
                        zip = "05401"
                    },
                phone_number = new Phone_Number()
                    {
                        area_code = "340",
                        first = "642",
                        last = "4604"
                    }
            });
            sampleContacts.Add(new Person()
            {
                name = "John",
                address = new Address()
                {
                    house_number = "32",
                    street = "S Willard",
                    city = "Burlington",
                    state = "VT",
                    zip = "05402"
                },
                phone_number = new Phone_Number()
                {
                    area_code = "508",
                    first = "693",
                    last = "6789"
                }
            });





            Menu menu = new Menu() { address_book = new AddressBook() { contacts = sampleContacts } };
            menu.displayMainMenu();
        }
    }
}