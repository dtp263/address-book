﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace address_book
{
    interface IPerson
    {
        string toFlatText();
        string toXML();
    }


    public class Person : IPerson
    {
        public string name { get; set; }
        public Address address { get; set; }
        public Phone_Number phone_number { get; set; }

        public override string ToString()
        {
            return name + "\n" + address + "\n" + phone_number;
        }

        public string toFlatText()
        {
            return name + "\n" + address.toFlatText() + phone_number.toFlatText();
        }

        public string toXML()
        {
            return "<person>\n\t<name>" + name + "</name>\n\t" + address.toXML() + phone_number.toXML() + "</person>\n"; 
        }
    }
}
