﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace address_book
{
    public class Menu
    {
        public AddressBook address_book { get; set; }

        public Person newContactForm()
        {
            string newName, newCity, newState, newZip, newHouse, newStreet, newPhoneNumber;
            int n;

            Console.Write("Enter Name: ");
            newName = Console.ReadLine();
            Console.Write("Enter City: ");
            newCity = Console.ReadLine();
            Console.Write("Enter State: ");
            newState = Console.ReadLine();
            Console.Write("Enter Zipcode: ");
            newZip = Console.ReadLine();
            while (newZip.Length != 5 && int.TryParse(newZip, out n))
            {
                Console.Write("Im sorry, your zipcode must only have 5 digits.\n" +
                              "Enter Zipcode: ");
                newZip = Console.ReadLine();
            }
            Console.Write("Enter street (optional): ");
            newStreet = Console.ReadLine();
            Console.Write("Enter House Number (optional): ");
            newHouse = Console.ReadLine();
            Console.Write("Enter Phone Number (optional): ");

            newPhoneNumber = Console.ReadLine();
            var getPhoneNumbers = (from t in newPhoneNumber
                              where char.IsDigit(t)
                              select t).ToArray();
            newPhoneNumber = new string(getPhoneNumbers);
            // Handles if there is a (1) for Country code in the phone number
            if (newPhoneNumber[0] == 1)
                n = 1;
            else
                n = 0;

            Person newPerson = new Person()
            {
                name = newName,
                address = new Address()
                    {
                        house_number = newHouse,
                        street = newStreet,
                        city = newCity,
                        state = newState,
                        zip = newZip
                    },
                phone_number = new Phone_Number() 
                    { 
                        area_code = newPhoneNumber.Substring(0, 3),
                        first = newPhoneNumber.Substring(3, 3),
                        last = newPhoneNumber.Substring(6, 4)
                    }
            };

            Console.Clear();
            Console.Write("Does this look correct?\n\n" + newPerson + "\n\n(y/n)");
            string choice = Console.ReadLine();

            if (choice == "y")
                return newPerson;
            else
                newContactForm();
            return null;
        }

        public void displayMainMenu()
        {
            Console.Write("1. Add a new Contact.\n" +
                          "2. Search for a Contact.\n" +
                          "3. Display all Contacts.\n" +
                          "4. Exit.\n");
            string choice = Console.ReadLine();
            switch (choice)
            {
                case "1":
                    Console.Clear();
                    Person newPerson = newContactForm();
                    if (newPerson == null)
                    {
                        Console.Write("Im sorry there seems to have been a problem...");
                        Console.ReadLine();
                    }
                    else
                        address_book.addContact(newPerson);
                    displayMainMenu();
                    break;
                case "2":
                    Console.Clear();
                    this.displaySearchMenu();
                    break;
                case "3":
                    Console.Clear();
                    displayPeople(address_book.contacts);
                    displayMainMenu();
                    break;
                case "4":
                    Console.Clear();
                    break;
                default:
                    break;
            }
        }

        public void displaySearchMenu()
        {
            Console.Write("Search by:\n" +
                          "  1. Name.\n" +
                          "  2. Address.\n" +
                          "  3. Phone Number.\n" +
                          "  4. Go Back.\n");
            string choice = Console.ReadLine();
            string query;
            List<Person> results;
            switch (choice)
            {
                case "1":
                    Console.Clear();
                    Console.Write("Please enter a name: ");
                    query = Console.ReadLine();
                    results = address_book.searchContacts(query, "name");
                    displayPeople(results);
                    break;
                case "2":
                    Console.Clear();
                    displayAddressSearchMenu();
                    break;
                case "3":
                    Console.Clear();
                    Console.Write("Please enter a Phone Number: ");
                    query = Console.ReadLine();
                    results = address_book.searchContacts(query, "phone_number");
                    displayPeople(results);
                    break;
                case "4":
                    Console.Clear();
                    displayMainMenu();
                    break;
            }
        }

        public void displayAddressSearchMenu()
        {
            Console.Write("Search by:\n" +
              "  1. City.\n" +
              "  2. State.\n" +
              "  3. Zip.\n" +
              "  4. Go Back.\n");
            string choice = Console.ReadLine();
            string query;
            List<Person> results;
            switch (choice)
            { 
                case "1":
                    Console.Clear();
                    Console.Write("Please enter the city: ");
                    query = Console.ReadLine();
                    results = address_book.searchContacts(query, "city");
                    displayPeople(results);
                    break;
                case "2":
                    Console.Clear();
                    Console.Write("Please enter the state: ");
                    query = Console.ReadLine();
                    results = address_book.searchContacts(query, "state");
                    displayPeople(results);
                    break;
                case "3":
                    Console.Clear();
                    Console.Write("Please enter the zip: ");
                    query = Console.ReadLine();
                    results = address_book.searchContacts(query, "zip");
                    displayPeople(results);
                    break;
                case "4":
                    Console.Clear();
                    displaySearchMenu();
                    break;
                
            }
        }

        public void displayPeople(List<Person> results)
        {
            foreach (Person person in results)
            {
                Console.Write(person + "\n\n");
            }
            Console.Write("Press any key to return to main menu...");
            Console.ReadKey();
            Console.Clear();
            displayMainMenu();
        }
    }
}
