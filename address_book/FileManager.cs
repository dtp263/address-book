﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace address_book
{
    interface IFileManager
    {
        List<Person> readXML(string filename);
        List<Person> readFlatText(string filename);

        void writeXML(string filename, List<Person> contacts);
        void writeFlat(string filename, List<Person> contacts);
    }

    public class FileManager : IFileManager
    {
        public List<Person> readXML(string filename)
        { 
            // NOT FINISHED
            return null;  
        }

        public List<Person> readFlatText(string filename)
        {
            // NOT FINISHED
            string text = System.IO.File.ReadAllText(filename);
            return null;
        }





        public void writeXML(string filename, List<Person> contacts)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename))
            {
                foreach (Person person in contacts)
                {
                        file.WriteLine(person.toXML());
                }
            }
        }
        public void writeFlat(string filename, List<Person> contacts)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename))
            {
                foreach (Person person in contacts)
                {
                    file.WriteLine(person.toFlatText());
                }
            }
        }
    }
}
