﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace address_book
{
    interface IPhone_Number
    {
        string toFlatText();
        string toXML();
    }


    public class Phone_Number : IPhone_Number
    {
        public string area_code { get; set; }
        public string first { get; set; }
        public string last { get; set; }

        public override string ToString()
        {
            return "(" + area_code + ") " + first + "-" + last;
        }

        public string toFlatText()
        {
            return area_code + "\n" + first + "\n" + last + "\n";
        }

        public string toXML()
        {
            return "\t<phone_number>\n\t\t<area_code>" + area_code + "</area_code>\n\t\t<first>" + first + "</first>\n\t\t<last>" + last + "</last>\n\t<phone_number>\n";
        }
    }
}
